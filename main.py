import time
import concurrent.futures
import threading
import multiprocessing
import asyncio



def cpu_bound_function(number):
    while number > 0:
        number -= 1


def io_bound_function(seconds):
    time.sleep(seconds)

async def async_io_bound_function(seconds):
    await asyncio.sleep(seconds)


def synchronous_function(param_list, func):
    for x in param_list:
        func(x)


def threaded_function(param_list, func):
    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        executor.map(func, param_list)


def multi_process_function(param_list, func):
    with multiprocessing.Pool(processes=4) as pool:
        pool.map(func, param_list)


async def asyncio_function(param_list):
    await asyncio.gather(*(io_bound_function(param) for param in param_list))


if __name__ == '__main__':
    number_list = [1e8, 1e8, 1e8, 1e8]
    sleep_list = [3, 2, 3, 3]

    start_time = time.time()
    synchronous_function(number_list, cpu_bound_function)
    print(f'Synchronous CPU-bound function took {time.time() - start_time} seconds!')

    start_time = time.time()
    threaded_function(number_list, cpu_bound_function)
    print(f'Threaded CPU-bound function took {time.time() - start_time} seconds!')

    start_time = time.time()
    multi_process_function(number_list, cpu_bound_function)
    print(f'Multiprocessing CPU-bound function took {time.time() - start_time} seconds!')


    start_time = time.time()
    synchronous_function(sleep_list, io_bound_function)
    print(f'Synchronous IO-bound function took {time.time() - start_time} seconds!')

    start_time = time.time()
    threaded_function(sleep_list, io_bound_function)
    print(f'Threaded IO-bound function took {time.time() - start_time} seconds!')

    start_time = time.time()
    multi_process_function(sleep_list, io_bound_function)
    print(f'Multiprocessing IO-bound function took {time.time() - start_time} seconds!')

    start_time = time.time()
    multi_process_function(sleep_list, io_bound_function)
    print(f'Asyncio IO-bound function took {time.time() - start_time} seconds!')

